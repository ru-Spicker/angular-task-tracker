export interface User {
  email: string;
  password: string;
  returnSecureToken: boolean;
}

export interface FbAuthResponse {
  idToken: string;
  expiresIn: string;
}

export interface Task {
  id?: string;
  title: string;
  description: string;
  priority?: number;
  status: 'Создана' | 'Назначена' | 'В работе' | 'Просрочена' | 'Выполнена';
  executor?: string;
  created: Date;
  intend?: number;
  consume?: number;
  // название, описание, дата, приоритет,
  // планируемое и затраченное время, статус выполнения
}

export interface FbCreateResponse {
  name: string;
}
