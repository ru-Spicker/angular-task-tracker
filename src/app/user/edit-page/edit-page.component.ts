import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {TaskService} from '../shared/services/task.service';
import {switchMap} from 'rxjs/operators';
import {Task} from '../../shared/interfaces';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {AlertService} from '../shared/services/alert.service';

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss']
})
export class EditPageComponent implements OnInit, OnDestroy {
  form: FormGroup;
  submitted = false;
  task: Task;
  uSub: Subscription;
  constructor(
    private route: ActivatedRoute,
    private taskService: TaskService,
    private alert: AlertService
  ) { }

  ngOnInit() {
    this.route.params.pipe(
      switchMap((params: Params) => {
      return this.taskService.getById(params.id);
    })).subscribe((task: Task) => {
      this.task = task;
      this.form = new FormGroup({
        title:        new FormControl(task.title, [Validators.required]),
        description:  new FormControl(task.description, [Validators.required]),
        intend: new FormControl((task.intend ? task.intend : ''), [Validators.required])
      });
    });
  }
  ngOnDestroy(): void {
    if (this.uSub) {this.uSub.unsubscribe(); }
  }

  submit() {
    if (this.form.invalid) { return; }
    this.submitted = true;
    const editedTask = this.task;
    editedTask.title = this.form.value.title;
    editedTask.description = this.form.value.description;
    editedTask.intend = this.form.value.intend;
    this.uSub = this.taskService.update(editedTask).subscribe( (response: Task) => {
      this.submitted = false;
      this.alert.warning('Пост отредактирован');
    });
  }
}
