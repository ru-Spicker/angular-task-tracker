import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Task} from '../../shared/interfaces';
import {TaskService} from '../shared/services/task.service';
import {AlertService} from '../shared/services/alert.service';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss']
})
export class CreatePageComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  executors = [
    {username: 'vuser', email: 'v@mail.ru' },
    {username: 'xuser', email: 'x@mail.ru' },
    {username: 'yuser', email: 'y@mail.ru' },
  ];
  constructor(
    private taskService: TaskService,
    private alert: AlertService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      intend: new FormControl('', [Validators.required]),
      priority: new FormControl('', [Validators.required]),
      executor: new FormControl('', [Validators.required]),
    });
  }

  submit() {
    if (this.form.invalid) { return; }
    const task: Task = {
      title: this.form.value.title,
      description: this.form.value.description,
      created: new Date(),
      status: 'Создана',
      intend: this.form.value.intend,
      priority: this.form.value.priority,
      executor: this.form.value.executor,
      consume: 0
    };
    this.taskService.create(task).subscribe( (res) => {
      this.form.reset();
      this.alert.success('Пост создан');

    });
  }
}
