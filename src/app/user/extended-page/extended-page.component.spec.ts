import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendedPageComponent } from './extended-page.component';

describe('ExtendedPageComponent', () => {
  let component: ExtendedPageComponent;
  let fixture: ComponentFixture<ExtendedPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtendedPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendedPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
