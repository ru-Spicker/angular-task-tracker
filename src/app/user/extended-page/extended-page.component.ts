import { Component, OnInit } from '@angular/core';
import {TaskService} from '../shared/services/task.service';
import {Task} from '../../shared/interfaces';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-extended-page',
  templateUrl: './extended-page.component.html',
  styleUrls: ['./extended-page.component.scss']
})
export class ExtendedPageComponent implements OnInit {
  tasks$: Observable<Task[]>;
  constructor(private taskService: TaskService) { }

  ngOnInit() {
    this.tasks$ = this.taskService.getAll();
  }

}
