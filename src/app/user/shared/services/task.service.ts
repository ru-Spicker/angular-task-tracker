import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {FbCreateResponse, Task} from '../../../shared/interfaces';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable()
export class TaskService {
  constructor(private http: HttpClient) {
  }
  create(task: Task): Observable<Task> {
    return this.http.post(`${environment.fbDbUrl}/tasks.json`, task)
      .pipe(map((response: FbCreateResponse) => {
        return {
          ...task,
          id: response.name
        };
    }));
  }
  getAll(): Observable<Task[]> {
    return this.http.get(`${environment.fbDbUrl}/tasks.json`)
      .pipe(
        map( (response: {[key: string]: any}) => {
        return Object.keys(response)
          .map( (key) => ({
            ...response[key]
            , id: key
            , created: new Date(response[key].created)
          }));
    }));
  }

  remove(id: string): Observable<any> {
    return this.http.delete(`${environment.fbDbUrl}/tasks/${id}.json`);
  }

  getById(id: string): Observable<Task> {
    return this.http.get<Task>(`${environment.fbDbUrl}/tasks/${id}.json`)
    .pipe(
        map( (task: Task) => {
          return {
                ...task, id,
                created: new Date(task.created),
          };
        }
    ));
  }
  update(task: Task): Observable<Task> {
    return this.http.patch<Task>(`${environment.fbDbUrl}/tasks/${task.id}.json`, task);
  }
}
