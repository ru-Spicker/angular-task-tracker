import {Pipe, PipeTransform} from '@angular/core';
import {Task} from '../../../shared/interfaces';

@Pipe({
  name: 'searchTask'
})

export class SearchPipes implements PipeTransform {
  transform(tasks: Task[], search = ''): Task[] {
    if (!search.trim()) return tasks;
    return tasks.filter( (task) => task.title.toLowerCase().includes(search.toLowerCase()));
  }

}
