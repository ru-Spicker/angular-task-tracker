import {NgModule, Provider} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import { UserLayoutComponent } from './shared/components/user-layout/user-layout.component';
import {LoginPageComponent} from './login-page/login-page.component';
import {BriefPageComponent} from './brief-page/brief-page.component';
import {ExtendedPageComponent} from './extended-page/extended-page.component';
import {CreatePageComponent} from './create-page/create-page.component';
import {EditPageComponent} from './edit-page/edit-page.component';
import {KanbanPageComponent} from './kanban-page/kanban-page.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from './shared/services/auth.service';
import {SharedModule} from './shared/shared.module';
import {AuthGuard} from './shared/services/auth.guard';
import {TaskService} from './shared/services/task.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthInterceptor} from '../shared/auth.interceptor';
import {SearchPipes} from './shared/pipes/search.pipes';
import {AlertService} from './shared/services/alert.service';

const INTERCEPTOR_PROVIDER: Provider = {
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: AuthInterceptor
};
@NgModule({
  declarations: [
    UserLayoutComponent,
    LoginPageComponent,
    BriefPageComponent,
    ExtendedPageComponent,
    KanbanPageComponent,
    CreatePageComponent,
    EditPageComponent,
    SearchPipes,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild([
      {path: '', component: UserLayoutComponent, children: [
          {path: '', redirectTo: '/user/login', pathMatch: 'full'},
          {path: 'login', component: LoginPageComponent},
          {path: 'brief', component: BriefPageComponent, canActivate: [AuthGuard]},
          {path: 'create', component: CreatePageComponent, canActivate: [AuthGuard]},
          {path: 'kanban', component: KanbanPageComponent, canActivate: [AuthGuard]},
          {path: 'extended/:id', component: ExtendedPageComponent, canActivate: [AuthGuard]},
          {path: 'edit/:id', component: EditPageComponent, canActivate: [AuthGuard]},
        ]}
    ])
  ],
  exports: [RouterModule],
  providers: [AuthService, AuthGuard, TaskService, INTERCEPTOR_PROVIDER, AlertService],
})
export class UserModule {

}
