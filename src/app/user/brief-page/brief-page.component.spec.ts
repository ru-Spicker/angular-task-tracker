import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BriefPageComponent } from './brief-page.component';
import {FormsModule} from '@angular/forms';
import {SearchPipes} from '../shared/pipes/search.pipes';
import {RouterTestingModule} from '@angular/router/testing';
import {TaskService} from '../shared/services/task.service';
import {HttpClient, HttpClientModule, HttpHandler} from '@angular/common/http';

describe('BriefPageComponent', () => {
  let component: BriefPageComponent;
  let fixture: ComponentFixture<BriefPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BriefPageComponent, SearchPipes],
      imports: [ FormsModule, RouterTestingModule ],
      providers: [TaskService, HttpClient, HttpClientModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BriefPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
