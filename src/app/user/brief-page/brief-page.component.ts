import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {TaskService} from '../shared/services/task.service';
import { Task } from 'src/app/shared/interfaces';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-brief-page',
  templateUrl: './brief-page.component.html',
  styleUrls: ['./brief-page.component.scss']
})
export class BriefPageComponent implements OnInit, OnDestroy {
  tasks: Task[] = [];
  tSub: Subscription;
  rSub: Subscription;
  @Input() searchStr = '';
  constructor(private taskService: TaskService) { }

  ngOnInit() {
    this.tSub = this.taskService.getAll().subscribe( (tasks) => {
      this.tasks = tasks;
    });
  }

  ngOnDestroy(): void {
    if (this.tSub) { this.tSub.unsubscribe(); }
    if (this.rSub) { this.rSub.unsubscribe(); }
  }
  remove(id: string) {
    this.rSub = this.taskService.remove(id).subscribe( () => {
      this.tasks = this.tasks.filter( task => task.id !== id);
    });
  }
}
